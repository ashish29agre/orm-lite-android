package in.ashish29agre.ormlite;

import in.ashish29agre.ormlite.helper.CustomOrmliteHelper;
import in.ashish29agre.ormlite.model.Blog;
import in.ashish29agre.ormlite.model.Comment;
import in.ashish29agre.ormlite.model.User;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import android.app.Activity;
import android.os.Bundle;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

public class OrmliteExample extends Activity {
	static final Logger log = Logger.getLogger(OrmliteExample.class.getName());
	CustomOrmliteHelper helper;
	// ConnectionSource connectionSource;
	// DAO's of each classes
	Dao<Blog, Integer> blogDao;
	Dao<User, Integer> userDao;
	Dao<Comment, Integer> commentDao;
	final int DATABASE_VERSION = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ormlite_example);
		// ConnectionSource connectionSource =
		// new AndroidConnectionSource(sqliteOpenHelper);

		try {
			helper = new CustomOrmliteHelper(this, "blogger.db", null,
					DATABASE_VERSION);
			userDao = helper.getDao(User.class);
			blogDao = helper.getDao(Blog.class);
			commentDao = helper.getDao(Comment.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		createUser();
	}

	private void createUser() {
		try {
			QueryBuilder<User, Integer> userQb = userDao.queryBuilder();
			userQb.where().eq("username", "Ashish Agre");
			PreparedQuery<User> preparedQuery = userQb.prepare();
			List<User> tempUser = userDao.query(preparedQuery);
			if (tempUser != null && tempUser.size() == 1) {
				log.info("User id Already exists");
			} else {
				long id = userDao.create(new User("Ashish Agre"));
				createBlog(id);
				log.info("New user created");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void createBlog(long userId) {
		User usr = new User();
		usr.setId(userId);
		Blog ormLiteBlog = new Blog();
		ormLiteBlog.setUser(usr);
		ormLiteBlog.setTitle("Using ormlite for the first time");
		ormLiteBlog
				.setContent("Object Relational Mapping Lite (ORM Lite) provides some lightweight functionality for persisting Java objects to SQL databases while avoiding the complexity and overhead of more standard ORM packages. It supports a number of SQL databases using JDBC and also supports Sqlite with native calls to Android OS database APIs. Documentation about how to configure ORMLite for Android specifically is available in the manual.");
		long id;
		try {
			id = blogDao.create(ormLiteBlog);
			log.info("Blog 1 " + id);
			User tmpU = new User();
			tmpU.setUsername("dasdassasa");
			ormLiteBlog.setUser(tmpU);
			id = blogDao.create(ormLiteBlog);
			log.info("Blog 2 " + id);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
