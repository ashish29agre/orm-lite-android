package in.ashish29agre.ormlite.helper;

import in.ashish29agre.ormlite.model.Blog;
import in.ashish29agre.ormlite.model.Comment;
import in.ashish29agre.ormlite.model.User;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class CustomOrmliteHelper extends OrmLiteSqliteOpenHelper {

	// public CustomOrmliteHelper(Context arg0, String arg1, CursorFactory arg2,
	// int arg3, InputStream arg4) {
	// super(arg0, arg1, arg2, arg3, arg4);
	// }
	Dao<Blog, Integer> blogDao;
	Dao<User, Integer> userDao;
	Dao<Comment, Integer> commentDao;

	public CustomOrmliteHelper(Context context, String databaseName,
			CursorFactory factory, int databaseVersion) {
		super(context, databaseName, factory, databaseVersion);
	}

	// public CustomOrmliteHelper(Context context, String databaseName,
	// CursorFactory factory, int databaseVersion, File configFile) {
	// super(context, databaseName, factory, databaseVersion, configFile);
	// }

	// public CustomOrmliteHelper(Context context, String databaseName,
	// CursorFactory factory, int databaseVersion, int configFileId) {
	// super(context, databaseName, factory, databaseVersion, configFileId);
	// }

	@Override
	public void onCreate(SQLiteDatabase database,
			ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, User.class);
			TableUtils.createTable(connectionSource, Blog.class);
			TableUtils.createTable(connectionSource, Comment.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database,
			ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			TableUtils.dropTable(connectionSource, User.class, true);
			TableUtils.dropTable(connectionSource, Blog.class, true);
			TableUtils.dropTable(connectionSource, Comment.class, true);
			onCreate(database, connectionSource);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// public Dao<User, Integer> getUserDao() throws SQLException {
	// if (userDao == null) {
	// userDao = getDao(User.class);
	// }
	// return userDao;
	// }
	//
	// public Dao<Blog, Integer> getBlogDao() throws SQLException {
	// if (blogDao == null) {
	// blogDao = getDao(Blog.class);
	// }
	// return blogDao;
	// }
	//
	// public Dao<Comment, Integer> getCommentDao() throws SQLException {
	// if (commentDao == null) {
	// commentDao = getDao(Comment.class);
	// }
	// return commentDao;
	// }
}
