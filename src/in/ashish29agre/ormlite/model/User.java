package in.ashish29agre.ormlite.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class User {
	public static final String USER_ID = "fk_user_id";
	@DatabaseField(generatedId = true)
	private Long id;
	@DatabaseField(canBeNull = false)
	private String username;

	private ForeignCollection<Blog> blogs;

	public User() {

	}

	public User(String username) {
		this.username = username;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ForeignCollection<Blog> getBlogs() {
		return blogs;
	}

	public void setBlogs(ForeignCollection<Blog> blogs) {
		this.blogs = blogs;
	}

}
