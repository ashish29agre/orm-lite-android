/**
 * 
 */
package in.ashish29agre.ormlite.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author ashish
 * 
 */
@DatabaseTable(tableName = "blogs")
public class Blog {
	public static final String BLOG_ID = "fk_blog_id";

	@DatabaseField(generatedId = true)
	private Long id;
	@DatabaseField(canBeNull = false)
	private String title;
	@DatabaseField(canBeNull = false)
	private String content;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = User.USER_ID)
	private User user;

	@ForeignCollectionField(eager = true)
	ForeignCollection<Comment> comments;

	public Blog() {
	}

	public Blog(User user, String title, String content) {
		this.user = user;
		this.title = title;
		this.content = content;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ForeignCollection<Comment> getComments() {
		return comments;
	}

	public void setComments(ForeignCollection<Comment> comments) {
		this.comments = comments;
	}

}
