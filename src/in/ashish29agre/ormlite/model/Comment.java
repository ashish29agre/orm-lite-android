package in.ashish29agre.ormlite.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "comments")
public class Comment {
	
	public static final String COMMENT_ID = "fk_comment_id";
	
	@DatabaseField(generatedId = true)
	private Long id;
	@DatabaseField(canBeNull = false)
	private String content;

	@DatabaseField(foreign = true, foreignAutoRefresh = true, canBeNull = false, columnName = Blog.BLOG_ID)
	private Blog blog;
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = User.USER_ID)
	private User user;

	public Comment(Blog blog, String content) {
		this.blog = blog;
		this.content = content;
	}

	public Comment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Blog getBlog() {
		return blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
